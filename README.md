sublime-vim-theme
================

A theme for SublimeText that mimics many of the default Vim syntax highlighting rules

Specail thanks to [@aziz](https://github.com/aziz/tmTheme-Editor) for providing much of the boiler-plate as well as an easy color picker.

installation
------------

Run `./install.sh`

